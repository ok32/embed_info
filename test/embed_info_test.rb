require 'test_helper'

EI = EmbedInfo

class EmbedInfoTest < Minitest::Test
  def setup
    EI.youtube_api_key = 'AIzaSyCslmUCJaInxV9_Em0oZb3CMmLlbJqxHvw'
  end

  def test_that_it_has_a_version_number
    refute_nil ::EmbedInfo::VERSION
  end

  ### Common Stuff

  def test_raises_if_no_provider_handles_url
    exc = assert_raises do
      url = "https://youtu.be/x-DgL49CFlM"
      EI.get(url, [])
    end

    assert_kind_of EI::NoProviderError, exc
    assert_equal "EmbedInfo: No matching provider found.", exc.to_s
  end

  ### YouTube ###

  def test_youtube_acceptable_urls
    urls = [
      "https://www.youtube.com/watch?v=x-DgL49CFlM",
      "https://youtu.be/x-DgL49CFlM",
      "http://www.youtube.com/watch?v=x-DgL49CFlM",
      "http://youtu.be/x-DgL49CFlM",
      "http://www.youtube.com/watch?v=x-DgL49CFlM/",
      "http://youtu.be/x-DgL49CFlM/",
      "http://youtube.com/watch?v=x-DgL49CFlM/",
    ]

    urls.each do |url|
      assert EI::YouTube.handles?(url)
    end
  end

  def test_youtube_video_unauth
    EI.youtube_api_key = 'NOTVALID'

    url = "https://www.youtube.com/watch?v=x-DgL49CFlM"

    VCR.use_cassette("youtube_forbidden") do
      exc = assert_raises do
        EI.get(url, [EI::YouTube])
      end

      assert_kind_of EI::ProviderError, exc
      assert_equal "YouTube: Unexpected HTTP response: 400 - Bad Request", exc.to_s
    end
  end

  def test_youtube_requires_key
    VCR.use_cassette("youtube_existing_video") do
      EI::YouTube.get_info("https://www.youtube.com/watch?v=IYW4iszVH3w", { youtube_api_key: "AIzaSyCslmUCJaInxV9_Em0oZb3CMmLlbJqxHvw" })

      exc = assert_raises(ArgumentError) do
        EI::YouTube.get_info("https://www.youtube.com/watch?v=IYW4iszVH3w", {})
      end

      assert_equal "YouTube: API key is required but not provided.", exc.message
    end
  end

  def test_youtube_video
    url = "https://www.youtube.com/watch?v=IYW4iszVH3w"

    VCR.use_cassette("youtube_existing_video") do
      video = EI.get(url, [EI::YouTube])

      assert_kind_of EI::MediaTypes::YouTubeVideo, video
      assert_equal :youtube, video.provider

      assert_equal 'IYW4iszVH3w', video.id
      assert_equal 'Induction Proofs', video.title
      assert_equal 'https://i.ytimg.com/vi/IYW4iszVH3w/hqdefault.jpg',video.thumbnail_url
      assert_equal 1823, video.duration
      assert_equal 'ProfRobBob', video.author
    end
  end

  ## Vimeo ###

  def test_vimeo_acceptable_urls
    urls = [
      "https://vimeo.com/13437819",
      "https://www.vimeo.com/13437819",
      "http://vimeo.com/13437819",
      "http://www.vimeo.com/13437819",
      "http://www.vimeo.com/13437819/",
    ]

    urls.each do |url|
      assert EI::Vimeo.handles?(url)
    end
  end

  def test_vimeo_video
    url = "https://vimeo.com/13437819"

    VCR.use_cassette("vimeo_existing_video") do
      video = EI.get(url, [EI::Vimeo])

      assert_kind_of EI::MediaTypes::VimeoVideo, video
      assert_equal :vimeo, video.provider

      assert_equal '13437819', video.id
      assert_equal 'The Road Trip Sign', video.title
      assert_equal 'https://i.vimeocdn.com/video/77118429_640.jpg', video.thumbnail_url
      assert_equal 11, video.duration
      assert_equal 'ok32', video.author
    end
  end

  ### SoundCloud ###

  def test_soundcloud_acceptable_urls
    # http://snd.sc/
    urls = [
      "http://soundcloud.com/dids42-59/didaf-benny-hill-1",
      "https://soundcloud.com/dids42-59/didaf-benny-hill-1",
      "http://snd.sc/dids42-59/didaf-benny-hill-1",
      "https://snd.sc/dids42-59/didaf-benny-hill-1"
    ]

    urls.each do |url|
      assert EI::SoundCloud.handles?(url)
    end
  end

  def test_souncloud_track
    url = "http://soundcloud.com/dids42-59/didaf-benny-hill-1"

    VCR.use_cassette("soundcloud_existing_track") do
      track = EI.get(url, [EI::SoundCloud])

      assert_kind_of EI::MediaTypes::SoundCloudAudio, track
      assert_equal :soundcloud, track.provider

      assert_equal '181765802', track.id
      assert_equal 'DIDAF: Benny Hill by Desert Island Discs 56-60', track.title
      assert_equal 'Desert Island Discs 56-60', track.author
    end
  end

  def test_soundcloud_nonexistent_url
    url = "http://soundcloud.com/i-dont-exist-ima-bad-URL"

    VCR.use_cassette("soundcloud_bad_url") do
      assert_raises EI::NotFoundError do
        EI.get(url, [EI::SoundCloud])
      end
    end
  end
end
