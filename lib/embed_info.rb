require "embed_info/version"
require "httparty"
require "iso8601"

module EmbedInfo
  class Error < StandardError; end
  class NoProviderError < Error; end
  class ProviderError < Error; end
  class NotFoundError < Error; end

  class << self
    attr_accessor :youtube_api_key
  end

  def self.get(url, providers)
    provider = providers.find do |p|
      p.handles?(url)
    end

    raise NoProviderError.new("EmbedInfo: No matching provider found.") unless provider

    options = {
      youtube_api_key: EmbedInfo.youtube_api_key
    }

    provider.get_info(url, options)
  end

  module MediaTypes
    class Video
      attr_reader :data

      def initialize(data)
        @data = data
      end

      ### main interface ###

      def id
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def title
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def provider
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def thumbnail_url
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      # duration in seconds
      def duration
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def author
        raise NotImplementedError.new("Must be implemented in the subclass")
      end
    end

    class YouTubeVideo < Video
      def id
        @data['id'].to_s
      end

      def title
        @data['snippet']['title']
      end

      def provider
        :youtube
      end

      def thumbnail_url
        @data['snippet']['thumbnails']['high']['url']
      end

      def duration
        ISO8601::Duration.new(@data['contentDetails']['duration']).to_seconds.to_i
      end

      def author
        @data['snippet']['channelTitle']
      end
    end

    class VimeoVideo < Video
      def id
        @data['video_id'].to_s
      end

      def title
        @data['title']
      end

      def provider
        :vimeo
      end

      def thumbnail_url
        @data['thumbnail_url']
      end

      def duration
        @data['duration']
      end

      def author
        @data['author_name']
      end
    end

    ### Audio ###

    class Audio
      attr_reader :data

      def initialize(data)
        @data = data
      end

      ### main interface ###

      def id
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def title
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def provider
        raise NotImplementedError.new("Must be implemented in the subclass")
      end

      def author
        raise NotImplementedError.new("Must be implemented in the subclass")
      end
    end

    class SoundCloudAudio < Audio
      def initialize(data)
        super(data)

        @id = extract_id || (raise ProviderError.new("SoundCloud: couldn't find id in API response."))
      end

      def id
        @id
      end

      def title
        @data['title']
      end

      def provider
        :soundcloud
      end

      def author
        @data['author_name']
      end

      private
        def extract_id
          r = /api.soundcloud.com%2Ftracks%2F(\d+)/
          m = r.match(@data['html'])

          m && m[1]
        end
    end
  end

  module Providers
    HTTP_TIMEOUT = 5

    class YouTube
      include HTTParty

      base_uri 'https://www.googleapis.com/youtube/v3'
      default_timeout HTTP_TIMEOUT

      def self.handles?(url)
        !!get_id(url)
      end

      def self.get_id(url)
        video_id = '(?<id>[a-zA-Z0-9_-]+)'
        r = %r{
          ^(?:https?://)?(?:www\.)?
          (
             (youtube\.com/watch\?v=#{video_id})
            |(youtu\.be/#{video_id})
            |(youtube\.com/embed/#{video_id})
            |(youtube\.com/v/#{video_id})
          )
          (?:|/)
        }x

        m = r.match(url)

        m[:id] if m
      end

      def self.get_info(url, options)
        id = get_id(url) || return
        api_key = options[:youtube_api_key] || (raise ArgumentError.new("YouTube: API key is required but not provided."))
        parts = %w{ snippet contentDetails }.join(',')

        resp =
          begin
            get("/videos?part=#{parts}&id=#{id}&key=#{api_key}")
          rescue StandardError => e
            raise ProviderError.new("YouTube: Unexpected error in HTTP client: #{e}")
          end

        if resp.code != 200
          raise ProviderError.new("YouTube: Unexpected HTTP response: #{resp.code} - #{resp.message}")
        end

        data = JSON.parse(resp.body)

        raise NotFoundError.new("YouTube: No videos found with id #{id}.") if data['items'].empty?

        MediaTypes::YouTubeVideo.new(data['items'].first)
      end
    end


    class Vimeo
      include HTTParty

      base_uri 'https://vimeo.com/api'
      default_timeout HTTP_TIMEOUT

      def self.handles?(url)
        !!get_id(url)
      end

      def self.get_id(url)
        video_id = '(?<id>[0-9]+)'
        r = %r{
          ^(?:https?://)?(?:www\.)?
          (
            vimeo\.com/#{video_id}
          )
          (?:|/)
        }x

        m = r.match(url)

        m[:id] if m
      end

      def self.get_info(url, options)
        id = get_id(url) || return

        resp =
          begin
            get("/oembed.json?url=https://vimeo.com/#{id}")
          rescue StandardError => e
            raise ProviderError.new("Vimeo: Unexpected error in HTTP client: #{e}")
          end

        if resp.code == 404
          raise NotFoundError.new("Vimeo: No videos found with id #{id}.")
        elsif resp.code != 200
          raise ProviderError.new("Vimeo: Unexpected HTTP response: #{resp.code} - #{resp.message}")
        end

        MediaTypes::VimeoVideo.new(JSON.parse(resp.body))
      end
    end

    class SoundCloud
      include HTTParty

      base_uri 'https://soundcloud.com/'
      default_timeout HTTP_TIMEOUT

      def self.handles?(url)
        r = %r{
          ^(?:https?://)?(?:www\.)?
          (
             (soundcloud\.com/(.+))
            |(snd.sc/(.+))
          )
          (?:|/)
        }x

        !!r.match(url)
      end

      def self.get_info(url, options)
        resp =
          begin
            get("/oembed?url=#{url}&format=json")
          rescue StandardError => e
            raise ProviderError.new("SoundCloud: Unexpected error in HTTP client: #{e}")
          end

        if resp.code == 404
          raise NotFoundError.new("SoundCloud: No tracks found with url #{url}.")
        elsif resp.code != 200
          raise ProviderError.new("SoundCloud: Unexpected HTTP response: #{resp.code} - #{resp.message}")
        end

        MediaTypes::SoundCloudAudio.new(JSON.parse(resp.body))
      end
    end
  end

  # some reexporting's going on
  YouTube = Providers::YouTube
  Vimeo = Providers::Vimeo
  SoundCloud = Providers::SoundCloud
  Video = MediaTypes::Video
end
